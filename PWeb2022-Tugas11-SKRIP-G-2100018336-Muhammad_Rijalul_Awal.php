<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr =   $passErr=   $prodiErr=  $semesterErr= "";
$name = $email = $gender = $comment = $website =    $pass=  $prodi= $semester= $warna0= $warna1= $warna2= $warna3= $warna4= $warna5="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
  }
    
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL"; 
    }
  }

  if (empty($_POST["pass"])) {
    $passErr = "Password is required";
  } else {
    $pass = test_input($_POST["pass"]);
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["prodi"])) {
    $prodiErr = "Prodi is required";
  } else {
    $prodi = test_input($_POST["prodi"]);
  }

  if (empty($_POST["semester"])) {
    $semesterErr = "Semester is required";
  } else {
    $semester = test_input($_POST["semester"]);
  }

  if (isset($_POST["warna0"])) {
    $warna0 = test_input($_POST["warna0"]);
  } if(isset($_POST["warna1"])) {
    $warna1 = test_input($_POST["warna1"]);
  } if(isset($_POST["warna2"])) {
    $warna2 = test_input($_POST["warna2"]);
  } if(isset($_POST["warna3"])) {
    $warna3 = test_input($_POST["warna3"]);
  } if(isset($_POST["warna4"])) {
    $warna4 = test_input($_POST["warna4"]);
  } if(isset($_POST["warna5"])) {
    $warna5 = test_input($_POST["warna5"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field.</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Name: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website: <input type="text" name="website" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Password: <input type="password" name="pass" value="<?php echo $pass;?>">
  <span class="error">* <?php echo $passErr;?></span>
  <br><br>
  Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  <br><br>
  Pilih Program Studi: <span class="error">* <?php echo $prodiErr;?></span><br/>
<select name="prodi">
    <option value=""> </option><br/>
    <option value="Teknik Informatika"> Teknik Informatika </option><br/>
    <option value="Teknik Industri"> Teknik Industri </option><br/>
    <option value="Teknik Elektro"> Teknik Elektro  </option><br/>
    <option value="Teknik Kimia"> Teknik Kimia </option><br/>
    <option value="Teknik Pangan"> Teknik Pangan </option><br/><br>
</select><br><br>
Semester: <span class="error">* <?php echo $semesterErr;?></span>
<select name="semester" size="4">
    <option value="<?php echo $name;?>"> </option><br/>
    <option value=""> </option><br/>
    <option value="Semester 1"> Semester 1 </option><br/>
    <option value="Semester 2"> Semester 2 </option><br/>
    <option value="Semester 3"> Semester 3 </option><br/>
    <option value="Semester 4"> Semester 4 </option><br/>
    <option value="Semester 5"> Semester 5 </option><br/>
    <option value="Semester 6"> Semester 6 </option><br/>
    <option value="Semester 7"> Semester 7 </option><br/>
    <option value="Semester 8"> Semester 8 </option><br/>
    <option value="Semester 9"> Semester 9 </option><br/>
    <option value="Semester 10"> Semester 10 </option><br/><br>
</select>
  <br><br>Favorit Warna:<br>
  <input type="checkbox" name="warna0" value="Tidak ada"> Tidak ada di list <br>
    <input type="checkbox" name="warna1" value="Hitam"> Hitam <br>
    <input type="checkbox" name="warna2" value="Putih"> Putih <br>
    <input type="checkbox" name="warna3" value="Merah"> Merah <br>
    <input type="checkbox" name="warna4" value="Hijau"> Hijau <br>
    <input type="checkbox" name="warna5" value="Biru"> Biru <br>
  <br><br>Gender:
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>

<?php
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
echo "<br>";
echo $pass;
echo "<br>";
echo $prodi;
echo "<br>";
echo $semester;
echo "<br>";
echo $warna0, $warna1,  $warna2,  $warna3,  $warna4,  $warna5;
echo "<br>";
echo $comment;
echo "<br>";
echo $gender;
?>

</body>
</html>